"use strict";
/**
 * PayNas Core API
 * This is a sample definitions file for the core api to Paynas
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 *
 * NOTE: This file is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the file manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var api = require("./api");
var config = {};
describe("AttendanceApi", function () {
    var instance;
    beforeEach(function () {
        instance = new api.AttendanceApi(config);
    });
    test("checkIn", function () {
        var body = undefined;
        return expect(instance.checkIn(body, {})).resolves.toBe(null);
    });
    test("checkOut", function () {
        var body = undefined;
        return expect(instance.checkOut(body, {})).resolves.toBe(null);
    });
    test("getCheckInOut", function () {
        return expect(instance.getCheckInOut({})).resolves.toBe(null);
    });
    test("historyList", function () {
        var page = 789;
        var month = 789;
        var year = 789;
        var search = "search_example";
        return expect(instance.historyList(page, month, year, search, {})).resolves.toBe(null);
    });
    test("manualCheckInOut", function () {
        var body = undefined;
        return expect(instance.manualCheckInOut(body, {})).resolves.toBe(null);
    });
});
describe("HolidayApi", function () {
    var instance;
    beforeEach(function () {
        instance = new api.HolidayApi(config);
    });
    test("createHoliday", function () {
        var body = undefined;
        return expect(instance.createHoliday(body, {})).resolves.toBe(null);
    });
    test("getHolidaysList", function () {
        var startDate = 2013 - 10 - 20, T19;
        30 + 01;
        00;
        var endDate = 2013 - 10 - 20, T19;
        30 + 01;
        00;
        var type = undefined;
        return expect(instance.getHolidaysList(startDate, endDate, type, {})).resolves.toBe(null);
    });
    test("getRemindersList", function () {
        return expect(instance.getRemindersList({})).resolves.toBe(null);
    });
});
describe("RequestApi", function () {
    var instance;
    beforeEach(function () {
        instance = new api.RequestApi(config);
    });
    test("acceptRequest", function () {
        var id = 789;
        return expect(instance.acceptRequest(id, {})).resolves.toBe(null);
    });
    test("cancelRequest", function () {
        var id = 789;
        return expect(instance.cancelRequest(id, {})).resolves.toBe(null);
    });
    test("myRequestsList", function () {
        var username = 789;
        var requestType = 789;
        var page = 789;
        var status = 789;
        return expect(instance.myRequestsList(username, requestType, page, status, {})).resolves.toBe(null);
    });
    test("rejectRequest", function () {
        var id = 789;
        return expect(instance.rejectRequest(id, {})).resolves.toBe(null);
    });
    test("requestsList", function () {
        var assigned = "assigned_example";
        var username = "username_example";
        var requestType = "requestType_example";
        var page = 789;
        var status = 789;
        return expect(instance.requestsList(assigned, username, requestType, page, status, {})).resolves.toBe(null);
    });
});
describe("UserApi", function () {
    var instance;
    beforeEach(function () {
        instance = new api.UserApi(config);
    });
    test("getList", function () {
        var startDate = 2013 - 10 - 20, T19;
        30 + 01;
        00;
        var endDate = 2013 - 10 - 20, T19;
        30 + 01;
        00;
        var type = "type_example";
        return expect(instance.getList(startDate, endDate, type, {})).resolves.toBe(null);
    });
    test("getUserBasicInfo", function () {
        var user = 789;
        return expect(instance.getUserBasicInfo(user, {})).resolves.toBe(null);
    });
    test("getUserNotificationList", function () {
        var page = 789;
        return expect(instance.getUserNotificationList(page, {})).resolves.toBe(null);
    });
    test("getUserPersonalInfo", function () {
        var user = 789;
        return expect(instance.getUserPersonalInfo(user, {})).resolves.toBe(null);
    });
    test("getUserWorkInfo", function () {
        var user = 789;
        return expect(instance.getUserWorkInfo(user, {})).resolves.toBe(null);
    });
    test("updateUserPersonalDetails", function () {
        var user = 789;
        var body = undefined;
        return expect(instance.updateUserPersonalDetails(user, body, {})).resolves.toBe(null);
    });
    test("userLogin", function () {
        var body = undefined;
        return expect(instance.userLogin(body, {})).resolves.toBe(null);
    });
    test("userLogout", function () {
        return expect(instance.userLogout({})).resolves.toBe(null);
    });
    test("userResetPassword", function () {
        var body = undefined;
        return expect(instance.userResetPassword(body, {})).resolves.toBe(null);
    });
    test("userResetPasswordChangePassword", function () {
        var body = undefined;
        return expect(instance.userResetPasswordChangePassword(body, {})).resolves.toBe(null);
    });
    test("userResetPasswordCode", function () {
        var body = undefined;
        return expect(instance.userResetPasswordCode(body, {})).resolves.toBe(null);
    });
});
